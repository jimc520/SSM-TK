<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>修改用户信息</title>
</head>
<body>
<fieldset>
    <legend>添加用户</legend>
    <form id="fom">
        <input type="hidden" name="id" value="${user.id}">
        <div><label>用户名称：<input type="text" name="username" value="${user.username}"></label></div>
        <div><label>用户密码：<input type="text" name="password" value="${user.password}"></label></div>
        <div><label>用户邮箱：<input type="text" name="email" value="${user.email}"></label></div>
        <div><input id="addBtn" type="button" value="提交"></div>
    </form>
</fieldset>
<script type="text/javascript" src="${ctxPath}/resources/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="${ctxPath}/resources/js/jquery.form.js"></script>
<script type="text/javascript">
    $(function () {
        $("#addBtn").click(function () {
            $('#fom').ajaxSubmit({
                url: "${ctxPath}/user/update",
                type: 'post',
                cache: false,
                dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        location.href = "${ctxPath}/user/listPage";
                        alert(data.msg);
                    } else {
                        alert(data.msg);
                    }
                }
            });
        });
    })

</script>
</body>
</html>
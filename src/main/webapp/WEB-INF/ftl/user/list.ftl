<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>用户信息列表展示</title>
</head>
<body>
<fieldset>
    <legend>用户列表</legend>
    <div>
        <a href="${ctxPath}/user/addPage">添加</a>
    </div>
    <div>
        <table border="1">
            <tr>
                <th>用户名</th>
                <th>密码</th>
                <th>邮箱</th>
                <th>操作</th>
            </tr>
            <#if users??>
                <#list users as user>
                    <tr>
                        <td>${user.username}</td>
                        <td>${user.password}</td>
                        <td>${user.email}</td>
                        <td><a href="${ctxPath}/user/updatePage?userId=${user.id}">修改</a>|<a href="javascript:del(${user.id});">删除</a></td>
                    </tr>
                </#list>
            </#if>
        </table>
    </div>
</fieldset>
<script type="text/javascript" src="${ctxPath}/resources/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript">
    function del(id) {
        var conf = confirm("确认删除？");
        if (conf){
            $.ajax({
                url: '${ctxPath}/user/delete',
                data: {
                    userId: id
                },
                type: 'post',
                cache: false,
                dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        location.href = '${ctxPath}/user/listPage';
                        alert(data.msg);
                    } else {
                        alert(data.msg);
                    }
                },
                error: function () {
                    // alert(data);
                }
            })
        }
    }
</script>
</body>
</html>
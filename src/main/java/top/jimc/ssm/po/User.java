package top.jimc.ssm.po;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;

@Table(name = "user")
public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    /*
     * 记住一个原则：实体类的字段数量 >= 数据库表中需要操作的字段数量。默认情况下，实体类中的所有字段都会作为表中的字段来操作，如果有额外的字段，必须加上@Transient注解。
     * 说明：
     *   表名默认使用类名,驼峰转下划线(只对大写字母进行处理),如UserInfo默认对应的表名为user_info。
     *   表名可以使用@Table(name = "tableName")进行指定,对不符合第一条默认规则的可以通过这种方式指定表名.
     *   字段默认和@Column一样,都会作为表字段,表字段默认为Java对象的Field名字驼峰转下划线形式.
     *   可以使用@Column(name = "fieldName")指定不符合第3条规则的字段名
     *   使用@Transient注解可以忽略字段,添加该注解的字段不会作为表字段使用.
     *   建议一定是有一个@Id注解作为主键的字段,可以有多个@Id注解的字段作为联合主键.
     *   如果是MySQL的自增字段，加上@GeneratedValue(generator = "JDBC")即可。
     */

    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    private String username;

    private String password;

    private String email;

    @Transient
    private String userId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
package top.jimc.ssm.mapper;

import tk.mybatis.mapper.common.Mapper;
import top.jimc.ssm.po.TestKeyUuid;

public interface TestKeyUuidMapper extends Mapper<TestKeyUuid> {
}
package top.jimc.ssm.mapper;

import tk.mybatis.mapper.common.Mapper;
import top.jimc.ssm.po.TestTable;

public interface TestTableMapper extends Mapper<TestTable> {
}
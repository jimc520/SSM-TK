package top.jimc.ssm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import top.jimc.ssm.common.base.controller.BaseController;

import java.util.Date;

/**
 * 模板引擎测试
 *
 * @author Jimc.
 * @since 2018/10/15.
 */
@Controller
@RequestMapping("test")
public class TemplateTestController extends BaseController {

    /**
     * 走Freemarker模板页面
     */
    @RequestMapping("test_ftl")
    public ModelAndView testFreemarker() {
        ModelAndView mv = new ModelAndView("test_ftl");
        mv.addObject("name", "freemarker");
        return mv;
    }

    /**
     * 走Velocity模板页面
     */
    @RequestMapping("test_vm")
    public ModelAndView testVelocity() {
        ModelAndView mv = new ModelAndView("test_vm");
        mv.addObject("page_title", "Test Page");  // 设置页面标题
        mv.addObject("now", new Date());
        mv.addObject("num", 232104953.12979978058145913);
        return mv;
    }

    /**
     * 走jsp页面
     */
    @RequestMapping("test_jsp")
    public ModelAndView test_jsp() {
        return new ModelAndView("test_jsp");
    }
}

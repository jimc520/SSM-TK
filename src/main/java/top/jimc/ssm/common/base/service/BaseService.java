package top.jimc.ssm.common.base.service;

import java.util.List;

/**
 * 通用Service接口
 * @author Jimc.
 * @since 2018/10/16.
 */
public interface BaseService<T> {

    T selectByKey(Object key);

    List<T> selectAll();

    int insert(T entity);

    int insertSelective(T entity);

    int delete(Object key);

    int updateAll(T entity);

    int updateNotNull(T entity);

    List<T> selectByExample(Object example);

    // TODO 其他...
}

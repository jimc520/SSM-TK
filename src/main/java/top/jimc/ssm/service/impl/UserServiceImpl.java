package top.jimc.ssm.service.impl;

import org.springframework.stereotype.Service;
import top.jimc.ssm.common.base.service.impl.BaseServiceImpl;
import top.jimc.ssm.po.User;
import top.jimc.ssm.service.UserService;

/**
 * @author Jimc.
 * @since 2018/10/12.
 */
@Service
public class UserServiceImpl extends BaseServiceImpl<User> implements UserService {
}

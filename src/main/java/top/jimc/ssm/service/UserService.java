package top.jimc.ssm.service;

import org.springframework.stereotype.Service;
import top.jimc.ssm.common.base.service.BaseService;
import top.jimc.ssm.po.User;

/**
 * @author Jimc.
 * @since 2018/10/12.
 */
@Service
public interface UserService extends BaseService<User> {
}

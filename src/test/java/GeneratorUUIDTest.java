import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import top.jimc.ssm.mapper.TestKeyUuidMapper;
import top.jimc.ssm.po.TestKeyUuid;

/**
 * @author Jimc.
 * @since 2018/10/16.
 */
public class GeneratorUUIDTest extends BaseJunit4Test {

    @Autowired
    private TestKeyUuidMapper testKeyUuidMapper;

    @Test
    public void insert(){
        TestKeyUuid testKeyUuid = new TestKeyUuid();
        testKeyUuid.setName("jimc");
        testKeyUuidMapper.insert(testKeyUuid);
    }
}
